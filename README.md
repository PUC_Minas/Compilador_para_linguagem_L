# Compilador para linguagem L

**Progresso:** CONCLUÍDO<br />
**Autor:** Paulo Victor de Oliveira Leal, Patricia Lima, e Jorge Moura<br />
**Data:** 2017<br />

### Objetivo
Implementação de compilador completo para uma linguagem simples desenvolvida pelo professor.

### Observação

IDE:  [Visual Studio Code](https://code.visualstudio.com/)<br />
Linguagem: [JAVA](https://www.java.com)<br />
Banco de dados: Não utiliza<br />

### Execução

    $ cd Fonte/
    $ javac *.java
    $ java AnalisadorSintatico
    

### Contribuição

Esse projeto está concluído e é livre para o uso.

## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->